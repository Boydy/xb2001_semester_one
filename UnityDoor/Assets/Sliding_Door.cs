﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sliding_Door : MonoBehaviour
{
    public Transform door1tran;
    public Transform door2tran;
    public float moveAmount = 1f;
    public float snap = 0.02f;
    public float speed = 5f;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider Col) //Open door
        
    {
        if (Col.gameObject.tag == "Player")
        {
            Debug.Log("Player in trigger");
            //door1tran.localPosition = new Vector3(moveAmount, 0, 0);
            //door2tran.localPosition = new Vector3(-moveAmount, 0, 0);
            StopCoroutine("Doormove");
            StartCoroutine("Doormove",moveAmount);
        }
           
    }
    private void OnTriggerExit(Collider Col) // Close door
        
    {
        if (Col.gameObject.tag == "Player")
        {
            Debug.Log("player exited trigger");
            //door1tran.localPosition = Vector3.zero;
            //door2tran.localPosition = Vector3.zero;
            StopCoroutine("Doormove");
            StartCoroutine("Doormove", 0f);
            
        }
        

    }
    IEnumerator Doormove(float target)
    {
        float xpos = door1tran.localPosition.x;
        Debug.Log("Started");
        while (xpos < (target - snap) || xpos > (target + snap)) 
        {
            Debug.Log("moving");
            xpos = Mathf.Lerp(door1tran.localPosition.x, target, speed * Time.deltaTime);
            door1tran.localPosition = new Vector3(xpos , 0, 0);
            door2tran.localPosition = new Vector3(-xpos, 0, 0);
            yield return null;
        }
        door1tran.localPosition = new Vector3(target, 0, 0);
        door2tran.localPosition = new Vector3(-target, 0, 0);
        Debug.Log("finished");
        yield return null;


     }

}
    
       
    

